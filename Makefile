main: main.cpp ann.o
	g++ -pedantic -g -o ann main.cpp ann.o
ann.o: ann.h ann.cpp
	g++ -pedantic -g -c ann.cpp
clean:
	rm *.o
