#ifndef ANN_H
#define ANN_H

#include <vector>
using namespace std;

class Ann {
  public:
    Ann();
    vector<double> encode(int);
    void backtrack (int);
    vector <double> forward_prop(int);
    double distance (vector<double>, vector<double>);
    vector< int > m_struct, m_train_out, m_test_out;
    vector< vector<double> > m_train_in, m_test_in, m_dummy;
    double m_alpha;
    vector< vector< vector<double> > > m_weights;
  private:
    class Node {
      public:
        Node();
    };
};

#endif
