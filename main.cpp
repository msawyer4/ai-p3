#include <iostream>
#include <iomanip>
#include <fstream>
//#include "encodings.cpp"
//#include "node.h"
#include <vector>
#include <cstdlib>
#include <sstream>
#include <algorithm>
#include "ann.h"

using namespace std;

int main(int argc, char **argv) {
  Ann ann;
  if (argc != 8) {
    cerr<<"USAGE: ./ann train_input.txt train_output.txt test_input.txt"
        <<" test_output.txt structure.txt weights.txt k"<<endl;
    return 1;
  }

  // File IO
  ofstream output_file;
  ifstream input_train_f;
  ifstream input_test_f;
  ifstream output_train_f;
  ifstream output_test_f;
  ifstream structure_f;
  ifstream weights_f;

  // Open Inputs
  output_file.open("ann_output.txt");
  input_train_f.open(argv[1]);
  output_train_f.open(argv[2]);
  input_test_f.open(argv[3]);
  output_test_f.open(argv[4]);
  structure_f.open(argv[5]);
  weights_f.open(argv[6]);

  //variables
  int k = atoi(argv[7]);

    //Train_Input.txt
  vector <vector <double> > training_in;
  string train_line;
//  cout<<"TRAIN_INPUT"<<endl;
  while (input_train_f.good()) {
    getline(input_train_f, train_line);
    stringstream ss (train_line);
    vector <double> v_d;
    double d;
    while (ss >> d) {
//      cout<<d<<' ';
      v_d.push_back(d);
    }
//    cout<<endl;
    training_in.push_back(v_d);
  }
  ann.m_train_in = training_in;

    //Test_Input.txt
  vector <vector <double> > testing_in;
  string test_line;
//  cout<<"TEST_INPUT"<<endl;
  while (input_test_f.good()) {
    getline(input_test_f, test_line);
    stringstream ss (test_line);
    vector <double> v_d;
    double d;
    while (ss >> d){
//      cout<<d<<' ';
      v_d.push_back(d);
    }
//    cout<<endl;
    testing_in.push_back(v_d);
  }
  ann.m_test_in = testing_in;

    //Train_Output.txt
  int out;
  vector<int> train_output;
//  cout<<"TRAIN_OUTPUT"<<endl;
  while(output_train_f>>out) {
    train_output.push_back(out);
//    cout<<out<<endl;
  }
  ann.m_train_out = train_output;

    //Test_Output.txt
  vector<int> test_output;
//  cout<<"TEST_OUTPUT"<<endl;
  while(output_test_f>>out) {
    test_output.push_back(out);
//    cout<<out<<endl;
  }
  ann.m_test_out = test_output;

    //Structure.txt
  vector<int>structure;
//  cout<<"STRUCTURE"<<endl;
  while (structure_f.good()) {
    string l;
    int level;
    structure_f>>l;
    stringstream ss (l);
    ss >> level;
//    cout<<level<<endl;
    structure.push_back(level);
  }
  ann.m_struct = structure;

    //Weights.txt
  vector < vector < vector <double > > > weights;
  for (int i = 0; i< structure.size(); i++) {
//    cout<<"Layer: "<<i<<endl;
    vector < vector<double> > node;
    for (int j=0; j< structure[i]; j++) {
//      cout<<"Node: "<<j<<endl;
      string row;
      double d;
      getline(weights_f, row);
      stringstream ss(row);
      vector <double> weight;
      while (ss>>d) {
//        cout<<d<<' ';
        weight.push_back(d);
      }
//      cout<<endl;
      node.push_back(weight);
    }
    weights.push_back(node);
  }
  ann.m_weights=weights;
//  cout<<"OUTPUT_NODES: "<<weights.size()<<endl;

  vector<vector <double> > dummy;
  for (int i = 0; i < structure.size()-1; i++) {
    vector <double> dummy_row;
    for (int j=0; j < structure[i]; j++) {
      dummy_row.push_back(0.01);
    }
    dummy.push_back(dummy_row);
  }
  ann.m_dummy=dummy;

  // K iterations
  for (int i=0; i<k; i++) {
//    cout<<"training_in.size() = "<<training_in.size()<<endl;
    for (int j=0; j<training_in.size()-1; j++) {
//      cout<<"==============================Iteration "<<j
//        <<"================================"<<endl;
      ann.backtrack(j);
    }
  }
  cout<<setprecision(16)<<fixed;
  for (int i=0; i<ann.m_struct.back(); i++)
    cout<<ann.m_weights[ann.m_weights.size()-3][0][i]<<endl;
  cout<<endl;
  // Test
  vector <vector <double> > distances;
  for (int i=0; i<ann.m_train_in.size()-1; i++) {
    vector <double> distance_row;
    for (int j = 0; j<ann.m_struct.back(); j++) {
      double d;
//      cout<<"testing_in["<<i<<"] = "<<ann.forward_prop(i)[j]<<endl;
//      cout<<"encoding["<<j<<"] = "<<ann.encode(j).back()<<endl;
      d = ann.distance(ann.forward_prop(i), ann.encode(j));
//      cout<<"d = "<<d<<endl;
      distance_row.push_back(d);
    }
    distances.push_back(distance_row);
  }

  // Find minimum, store in classification vector
  vector <int> classify;
//  cout<<"distances.size() = "<<distances.size()<<endl;
  for (int i=0; i<distances.size(); i++) {
    int min = 0;
    for (int j=0; j<distances[i].size(); j++) {
      if (distances[i][j] < distances[i][min])
        min = j;
    }
    classify.push_back(min);
    output_file<<min<<endl;
//    cout<<"Classificataion for input "<<i<<" = "<<min<<endl;
  }

  // Check validity of classification against test_output
  double accurate = 0;
  double totals = 0;
  for (int i=0; i<distances.size(); i++) {
    if (classify[i] == ann.m_test_out[i])
      accurate+=1.0;
    totals+=1.0;
  }
//  cout<<accurate<<" classifications correct."<<endl;
  cout<<setprecision(2)<<fixed;
  cout<<"Accuracy = "<<(accurate/totals)*100<<"%"<<endl;
}
