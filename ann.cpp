#include "ann.h"
#include <iostream>
#include <cmath>
using namespace std;

Ann::Ann() {
  m_alpha = 0.01;
}

vector <double> Ann::encode(int i) {
  // Takes int, returns vector of doubles
  // 2 => 0.9 0.1 0.9 0.9 ....
  // if i == it, 0.1, else, 0.9
  vector <double> d;
  for (int j=0; j<m_struct[m_struct.size()-2]; j++) {
    if (i==j)
      d.push_back(0.1);
    else
      d.push_back(0.9);
  }
  return d;
}

void Ann::backtrack(int iter) {
  vector<vector<double> > a;

  /* Step 1:
   * For each node in the input layer do
   * a_j = x_j
   * Already done by m_train_in.
   */
  vector<double> in_a;
//  cout<<"INPUT NODES: ";
  for (int i=0; i<m_train_in[0].size(); i++) {
//    cout<<m_train_in[iter][i]<<' ';
    in_a.push_back(m_train_in[iter][i]);
  }
//  cout<<endl;
  a.push_back(in_a);

  /* Step 2:
   * For each layer l=2..L do
   */
  for (int i=1; i<m_struct.size()-1; i++) {
    /* Step 3:
     * For each node j do
     *    in[j] = Sum of a[i]*w[i][j], Where i runs over all nodes that have
     *    edge to node j; this sum includes a_0*w_0j, where a_0 = 1 (always)
     *    and a dummy weight from dummy node 0 to node j(w_0j changes).
     *
     *                   1
     *        a_j = ------------
     *              1+e^(-in_j)
     */
    // Each node
    vector <double> in_a;
    for (int j=0; j < m_weights[i].size(); j++) {
      double a_j;
      double sum=0.0;
      // Each node coming into this node
      for (int k=0; k < m_weights[i-1].size(); k++) {
        sum += a[i-1][k]*m_weights[i-1][k][j];
      }
      sum += m_dummy[i][j];
//      cout<<"Layer: "<<i<<" Node: "<<j<<endl;
//      cout<<"sum = "<<sum<<endl;
      a_j = 1/(1+exp(-sum));
//      cout<<"A[i][j] = "<<a_j<<endl;
      in_a.push_back(a_j);
    }
    a.push_back(in_a);
  }

  /* Step 4:
   *    For each node in the OUTPUT layer calculate error.
   *    delta[j] = a[j](1-a[j])(y[j]-a[j])
   */
//  cout<<"DELTAS FOR OUTPUT LAYER: "<<endl;
  vector<vector <double> > delta;
  vector<double> d_nodes;
  for (int i=0; i< m_struct[m_struct.size()-1]; i++) {
    double a_j = a.back()[i];
    double y_j = encode(m_train_out[iter])[i];
    double delta_j = a_j*(1.0 - a_j)*(y_j - a_j);
//    cout<<"Node "<<i<<": "<<delta_j<<endl;
    d_nodes.push_back(delta_j);
  }
  delta.push_back(d_nodes);

  /* Step 5:
   *  For layers L-1 through 0, do:
   */
//  cout<<"Step 5: "<<endl;
  for (int i=m_struct.size()-3; i>=0; i--) {

    /* Step 6:
     * For each node in layer l, do:
     *    delta[i] = a[i](1-a[i])*SUM(delta[j]*m_weights[i-j])
     */
//    cout<<"Layer: "<<i<<endl;
    vector <double> d_nodes;
    for (int j=0; j<m_struct[i]; j++) {
      double a_i = a[i][j];
      double sum = 0;
      for (int k=0; k<m_struct[i+1]; k++) {
        sum += delta.back()[k]*m_weights[i][j][k];
      }
      double delta_i = a_i * (1-a_i) * sum;
//      cout<<"delta["<<j<<"] = "<<delta_i<<endl;
      d_nodes.push_back(delta_i);
    }
    delta.push_back(d_nodes);
  }

  /* Step 7:
   * For each weight weight[i->j] in network, do
   *
   *  Weight[i][j] = Weight(t-1)[i][j] + (alpha * A[i][j] * delta[j])
   */

//  cout<<"New Dummy Weights: "<<endl;
//  cout<<"m_dummy.size() = "<<m_dummy.size()<<endl;
  for (int i = 0; i < m_dummy.size(); i++) {
//    cout<<"Layer "<<i<<":"<<endl;
    for (int j = 0; j < m_dummy[i].size(); j++) {
//      cout<<m_dummy[i][j]<<" += "<<m_alpha<<" * 1 * "
//        <<delta[(m_dummy.size()-1)-i][j]<<endl;
//      cout<<"Node "<<j<<": ";
      m_dummy[i][j] += m_alpha * 1 * delta[(m_dummy.size()-1)-i][j];
//      cout<<m_dummy[i][j]<<endl;
    }
//    cout<<endl;
  }
//  cout<<"New Weights: "<<endl;
  for (int i = 0; i < m_weights.size()-2; i++) {
//    cout<<"Layer "<<i<<":"<<endl;
    for (int j = 0; j < m_weights[i].size(); j++) {
//      cout<<"Node "<<j<<": ";
      for (int k = 0; k < m_weights[i][j].size(); k++) {
        m_weights[i][j][k] += m_alpha * a[i][j] * delta[i][j];
//        cout<<m_weights[i][j][k]<<' ';
      }
//      cout<<endl;
    }
  }
}

vector<double> Ann::forward_prop(int iter) {
  vector<vector<double> > a;

  /* Step 1:
   * For each node in the input layer do
   * a_j = x_j
   * Already done by m_train_in.
   */
  vector<double> in_a;
//  cout<<"INPUT NODES: ";
  for (int i=0; i<m_test_in[0].size(); i++) {
//    cout<<m_test_in[iter][i]<<' ';
    in_a.push_back(m_test_in[iter][i]);
  }
//  cout<<endl;
  a.push_back(in_a);

  /* Step 2:
   * For each layer l = 2...L, do:
   */
  for (int i=1; i<m_struct.size()-1; i++) {

    /* Step 3:
     * For each node j do
     *
     *    in_j = SUM(a_i * w_ij) // where i runs over all nodes that have edge
     *    to node j; this sum includes a_0 * w_0j, a_0 = 1 (always) and a dummy      *    weight from dummy node 0 to node j (w_0j changes)
     *                1
     *    a_j = --------------
     *            1+e^(-in_j)
     */

    // Each node
    vector <double> in_a;
    for (int j=0; j < m_weights[i].size(); j++) {
      double a_j;
      double sum=0.0;
      // Each node coming into this node
      for (int k=0; k < m_weights[i-1].size(); k++) {
        sum += a[i-1][k]*m_weights[i-1][k][j];
      }
      sum += m_dummy[i][j];
//      cout<<"Layer: "<<i<<" Node: "<<j<<endl;
//      cout<<"sum = "<<sum<<endl;
      a_j = 1/(1+exp(-sum));
//      cout<<"A[i][j] = "<<a_j<<endl;
      in_a.push_back(a_j);
    }
    a.push_back(in_a);
  }
  return a[a.size()-1];
}

double Ann::distance(vector<double> a, vector<double> b) {
  double sum = 0.0;
  for (int i = 0; i<a.size(); i++) {
//    cout<<"a["<<i<<"] = "<<a[i]<<endl;
//    cout<<"b["<<i<<"] = "<<b[i]<<endl;
    sum += pow((a[i] - b[i]),2);
  }
  return sqrt(sum);
}
